#!/usr/bin/env bash

#stop all
docker-compose -f ./smart-contract/docker/docker-compose.yml down
docker-compose -f ./api/docker/docker-compose.yml down
docker-compose -f ./examples/docker/docker-compose.yml down