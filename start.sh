#!/usr/bin/env bash

# build
docker-compose -f ./smart-contract/docker/docker-compose.build.yml build --no-cache
docker-compose -f ./api/docker/docker-compose.build.yml build --no-cache
docker-compose -f ./examples/docker/docker-compose.build.yml build --no-cache

#deploy
docker-compose -f ./smart-contract/docker/docker-compose.yml up -d
docker-compose -f ./api/docker/docker-compose.yml up -d
docker-compose -f ./examples/docker/docker-compose.yml up -d
